
import discord
from discord import Message, client
from discord.utils import get
from discord.ext import commands
import re

# create préfix
bot = commands.Bot(command_prefix=['bot ', 'amelia '])
bot.remove_command("help")

# detected when the bot is online
@bot.event
async def on_ready():
    print("Bot is ready")
    # change bot presence
    await bot.change_presence(status=discord.Status.online, activity=discord.Game("/help"))

@bot.command()
async def ping(ctx):
    await ctx.send(f'My ping is {bot.latency}!')

@bot.command(pass_context=True)
async def bonjour(ctx):
    await ctx.send(f'Bonjour {ctx.message.author.name}')

@bot.command(pass_context=True)
async def ia(ctx, message):
    #if re.search(message,"toto"):
    if "toto" in message:
        await ctx.send(f'Behave yourself {ctx.message.author.name}')
    else:
        await ctx.send(f'error {ctx.message.author.name}')


# when the bot starting
print("bot is starting..")

# connect to server
bot.run("NzQ2MzI3NTQwMTg4MDUzNTM0.Xz-trw.HeAKDp_SuC8472IpKDyy6tZQGbQ")